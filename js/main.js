var icons = {
    hospital: {
        icon: 'common/img/hospital.png'
    },
    clinic: {
        icon: 'common/img/clinic.png'
    },
    psyHospital: {
        icon: 'common/img/therapy.png'
    }
};

var markerGroups = {
    "hospital": [],
    "clinic": [],
    "psyHospital": []
};

function initMap() {
	var uluru = {lat: 49.606889, lng: 34.548000};
    var map = new google.maps.Map(document.getElementById("map"), {
        center: uluru,
        zoom: 6
    });
   
    var xml = xmlParse(xmlMarkers);

    var markers = xml.documentElement.getElementsByTagName("marker");
    for (var i = 0; i < markers.length; i++) {
        var type = markers[i].getAttribute("type");
        var point = new google.maps.LatLng(
            parseFloat(markers[i].getAttribute("lat")),
            parseFloat(markers[i].getAttribute("lng")));
        var marker = createMarker(point, type, map);
    }
}

function createMarker(point, type, map) {
    var icon = icons[type] || {};
    var marker = new google.maps.Marker({
        map: map,
        position: point,
        icon: icon.icon,
        type: type
    });
    if (!markerGroups[type]) markerGroups[type] = [];
    markerGroups[type].push(marker);
    return marker;
}

function toggleGroup(type) {
    for (var i = 0; i < markerGroups[type].length; i++) {
        var marker = markerGroups[type][i];
        if (!marker.getVisible()) {
            marker.setVisible(true);
        } else {
            marker.setVisible(false);
        }
    }
}


var xmlMarkers = '<markers>' +
    '<marker lat="49.606878" lng="34.517997" type="hospital" />' +
    '<marker lat="47.908878" lng="35.941197" type="hospital" />' +
    '<marker lat="48.607878" lng="34.514497" type="clinic" />' +
    '<marker lat="49.105578" lng="35.145597" type="clinic" />' +
    '<marker lat="46.613378" lng="34.513397" type="psyHospital" />' +
    '<marker lat="49.412278" lng="33.542297" type="psyHospital" />' +
    '</markers>'
function xmlParse(str) {
    if (typeof ActiveXObject != 'undefined' && typeof GetObject != 'undefined') {
        var doc = new ActiveXObject('Microsoft.XMLDOM');
        doc.loadXML(str);
        return doc;
    }

    if (typeof DOMParser != 'undefined') {
        return (new DOMParser()).parseFromString(str, 'text/xml');
    }

    return createElement('div', null);
}